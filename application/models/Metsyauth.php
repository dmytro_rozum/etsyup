<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Metsyauth extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Подготовка post данных
     * @param $post
     * @return array
     */
    public function preparePostData($post){

        $arPost = array();
        foreach ($post as $key=>$value){
            $t = trim($value);
            if( isset($t) && !empty($t) ) {
                $t = stripslashes($t);
                $t = htmlspecialchars($t);
                $arPost[$key] = $t;
            }

        }

        if(count($arPost)>0) {
            return $arPost;
        } else {
            false;
        }

    }

    /**
     * Запрос кода на авторизацию
     * @return array|bool
     */
    public function AuthRequestToken(){

        $oauth = new OAuth(
            _ETSY_APP_KEY_,
            _ETSY_SECRET_
        );

        try {
            // make an API request for your temporary credentials
            $req_token = $oauth->getRequestToken(
                _ETSY_API_URL_ . 'oauth/request_token?scope=',
                "oob"
            );

            if( $req_token != false ) {
                return $req_token;
            } else {
                return false;
            }

        } catch (OAuthException $e) {

            return false;

        }

    }

    public function getAccessToken($post=array()){

        $oauth = new OAuth(
            _ETSY_APP_KEY_,
            _ETSY_SECRET_
        );

        $oauth->setToken($post['oauth_token'], $post['oauth_token_secret']);

        try {

            $acc_token = $oauth->getAccessToken(
                _ETSY_API_URL_ . 'oauth/access_token',
                null,
                $post['oauth_verifier']);

            if($acc_token != false){

                $oauth->setToken($acc_token['oauth_token'], $acc_token['oauth_token_secret']);

                $ar = array_merge($post,$acc_token);
                $this->insertAccessToken($ar);
                return $acc_token;

            } else {
                return false;
            }

        } catch (OAuthException $e) {
            return false;
        }

    }

    /**
     * Добавление access token в БД
     * @param $data
     */
    public function insertAccessToken($data){

        $data = array(
            'api_key' => _ETSY_APP_KEY_,
            'secret' => _ETSY_SECRET_,
            'oauth_token' => $data['oauth_token'],
            'oauth_token_secret' => $data['oauth_token_secret'],
            'login_url' => $data['oauth_login_url'],
            'code' => $data['oauth_verifier']
        );

        $acc_token = $this->getAccessTokenFromDB();
        if($acc_token!= false){
            $this->db->update('etsy_oauth', $data);
        } else {
            $this->db->insert('etsy_oauth', $data);
        }
    }

    /**
     * Получить acces token из БД
     * @return array/bool
     */
    public function getAccessTokenFromDB(){

        $q = $this->db->select('*')
            ->from('etsy_oauth')
            ->where('api_key', _ETSY_APP_KEY_)
            ->where('secret',_ETSY_SECRET_)
            ->get();

        $r = $q->result_array();

        if(count($r)>0){
            return $r[0];
        } else {
            return false;
        }

    }

}
?>