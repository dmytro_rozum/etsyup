<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mreceipt extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Синхронизауия данных
     * @param $data
     */
    public function sync($data){

        $r = $this->getByReceiptID($data['receipt_id']);
        if($r===false){
            $this->insert($data);
        } else {
            $this->update($r['id'],$data);
        }

    }

    /**
     * Добавить чек в БД
     * @param $data
     */
    public function insert($data){
        $this->db->insert('etsy_receipt', $data);
    }

    /**
     * Обновить чек по его id
     * @param $id
     * @param $data
     */
    public function update($id,$data){

        $this->db->where('id',$id);
        $this->db->insert('etsy_receipt', $data);
    }

    /**
     * Получить чек по внешнему id
     * @param $receipt_id
     * @return array/bool
     */
    public function getByReceiptID($receipt_id){

        $r =$this->db->select('*')
            ->from('etsy_receipt')
            ->where('receipt_id',$receipt_id)
            ->get()
            ->result_array();

        if(count($r)>0){
            return $r[0];
        } else {
            return false;
        }

    }

}