<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Форма ввода кода авторизации</title>
    <style>
        body *{
            font-family: Arial, Helvetica, Geneva sans-serif;
            font-size: 14px;
        }
        input[type=text]{
            padding: 6px;
            margin: 6px;
            border: 1px solid #eeeeee;
        }
        input[type=submit]{
            background: #009900;
            padding: 8px;
            border: 0;
            color: #ffffff;
            margin: 4px;
            text-transform: uppercase;
        }
    </style>
</head>
<body>

<div>
    <form action="<?=base_url();?>etsyauthverifier/" method="post">
        <div>Введите код</div>
        <div><input type="text" name="oauth_verifier" value=""/></div>
        <div><input type="submit" value="OK" name="etsy_oauth_submit"/></div>

        <input type="hidden" value="<?=$oauth_token;?>" name="oauth_token"/>
        <input type="hidden" value="<?=$oauth_token_secret;?>" name="oauth_token_secret"/>
        <input type="hidden" value="<?=$login_url;?>" name="oauth_login_url"/>
    </form>

    <p>
        Для получения кода перейдите по <a href="<?=$login_url;?>" target="_blank">ссылке</a>
    </p>
</div>

</body>
</html>