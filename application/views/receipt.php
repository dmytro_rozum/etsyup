<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>...</title>
    <style>
        body *{
            font-family: Arial, Helvetica, Geneva sans-serif;
            font-size: 14px;
        }
        input[type=text]{
            padding: 6px;
            margin: 6px;
            border: 1px solid #eeeeee;
        }
        input[type=submit]{
            background: #009900;
            padding: 8px;
            border: 0;
            color: #ffffff;
            margin: 4px;
            text-transform: uppercase;
        }
        th{
            text-align: left;
        }
    </style>
</head>
<body>

<div>
    <div>Всего: <?=$count;?></div>
    <div><a href="<?=base_url();?>/etsysyncreceipt/">Синхронизировать все чеки</a></div>
    <br/>
    <table width="100%">
        <tr>
            <th>receipt_id</th>
            <th>creation_tsz</th>
            <th>name</th>
            <th>buyer_email</th>
            <th>first_line</th>
            <th>second_line</th>
            <th>city</th>
            <th>state</th>
        </tr>
    <? foreach ($results as $item): ?>
        <tr>
            <td><?=$item['receipt_id'];?></td>
            <td><?=$item['creation_tsz'];?></td>
            <td><?=$item['name'];?></td>
            <td><?=$item['buyer_email'];?></td>
            <td><?=$item['first_line'];?></td>
            <td><?=$item['second_line'];?></td>
            <td><?=$item['city'];?></td>
            <td><?=$item['state'];?></td>
        </tr>
        <? /*print_r($item); break;*/ ?>
    <? endforeach; ?>
    </table>

</div>

</body>
</html>