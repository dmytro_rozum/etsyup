<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Etsyauthverifier extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
            'metsyauth'
        ));

    }

    public function index()
    {

        $post = $this->input->post();
        if (isset($post) && isset($post['etsy_oauth_submit'])) {

            $post = $this->metsyauth->preparePostData($post);
            if ($post !== false) {

                //var_dump($post); die();
                if (isset($post['oauth_verifier']) &&
                    !empty($post['oauth_verifier']) &&
                    isset($post['oauth_token']) &&
                    !empty($post['oauth_token']) &&
                    isset($post['oauth_token_secret']) &&
                    !empty($post['oauth_token_secret'] &&
                    isset($post['oauth_login_url']) &&
                    !empty($post['oauth_login_url']))
                ) {

                    $acc_token = $this->metsyauth->getAccessToken($post);
                    if($acc_token != false){
                        var_dump($acc_token);
                    } else {
                        $this->load->view('oauth/error');
                    }

                } else {
                    $this->load->view('oauth/error');
                }

            } else {
                $this->load->view('oauth/error');
            }

        } else {
            $this->load->view('oauth/error');
        }
    }
}

?>