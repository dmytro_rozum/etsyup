<?php defined('BASEPATH') OR exit('No direct script access allowed');

class C_test extends CI_Controller
{

    public $oauth;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        //$apiURI = 'shops/16150211/';
        //$apiURI = 'users/';
        //$apiURI = 'ShopAboutMember/';
        //$apiURI = 'shops/JaneRJewelry/receipts/';
        //$apiURI = 'shops/JaneRJewelry/';
        $apiURI = 'shops/9138057/receipts/';
        // /users/:user_id/shops
        $apiURL = _ETSY_API_URL_ . $apiURI . '?api_key=' . _ETSY_APP_KEY_;
        $postData = array(
            'key1' => 'value1',
            'key2' => 'value2'
        );

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header' =>
                    "Content-type: application/x-www-form-urlencoded\r\n" .
                    "X-RateLimit-Limit: 10000\r\n" .
                    "X-RateLimit-Remaining: 9924\r\n",
                'method' => 'GET',
                //'content' => http_build_query($postData)
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($apiURL, false, $context);
        if ($result === FALSE) {
            /* Handle error */
        }

        echo '<pre>';
        print_r(json_decode($result));
        echo '</pre>';

    }

    public function get_shop_by_user(){

        // Make sure you define API_KEY to be your unique, registered key
        $url = "https://openapi.etsy.com/v2/users/34140858/shops?api_key=" ._ETSY_APP_KEY_;

        //$apiURI = '/users/34140858/shops';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_body = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (intval($status) != 200){
            throw new Exception("HTTP $status\n$response_body");
        } else {
            echo '<pre>';
            print_r($response_body);
            echo '</pre>';
        }
    }

    public function verifier()
    {

        $oauth = new OAuth(
            _ETSY_APP_KEY_,
            _ETSY_SECRET_
        );

        $post = $this->input->post();

        if( isset($post['oauth_submit']) &&
            isset($post['verifier']) &&
            !empty($post['verifier']) ) {

            //$verifier = readline("Please enter verifier: ");
            $verifier = trim($post['verifier']);
            $oauth->setToken($post['oauth_token'], $post['oauth_token_secret']);

            try {
                $acc_token = $oauth->getAccessToken(_ETSY_API_URL_ . 'oauth/access_token', null, $verifier);
            } catch (OAuthException $e) {
                echo '<pre>';
                print_r($e);
                echo '</pre>';
                die();
            }

            $oauth_token = $acc_token['oauth_token'];
            $oauth_token_secret = $acc_token['oauth_token_secret'];

            try {
                $oauth->setToken($oauth_token, $oauth_token_secret);

                echo 'авторизация ok<br/>';

                echo '<pre>';
                print_r($acc_token);
                echo '</pre>';

                //print "Token: $oauth_token \n\n";
                //print "Secret: $oauth_token_secret \n\n";


                //$apiURI = 'shops/9138057/receipts/';
                //$apiURI = 'shops/9138057/receipts/';
                //$apiURI = '/users/34140858/shops';
                //$apiURL = "https://openapi.etsy.com/v2/users/__SELF__"; // work ok
                //$apiURL = _ETSY_API_URL_ . $apiURI . '?api_key=' . _ETSY_APP_KEY_;
                $apiURI = "https://openapi.etsy.com/v2/shops/9138057/receipts";


                $data = $oauth->fetch($apiURI, null, OAUTH_HTTP_METHOD_GET);
                $json = $oauth->getLastResponse();
                print_r(json_decode($json, true));

                /*
                try {
                    $data = $oauth->fetch($apiURI, null, OAUTH_HTTP_METHOD_GET);
                    $json = $oauth->getLastResponse();
                    print_r(json_decode($json, true));

                } catch (OAuthException $e) {
                    error_log($e->getMessage());
                    error_log(print_r($oauth->getLastResponse(), true));
                    error_log(print_r($oauth->getLastResponseInfo(), true));
                    exit;
                }*/


            } catch (OAuthException $e) {
                echo '<pre>';
                print_r($e);
                echo '</pre>';
                die();
            }

        } else {
            echo '!!!ошибка<br/>';
            echo '<a href="/index.php/c_test/oauth/">Попробывать еще раз</a>';
            echo '<pre>';
            print_r($post);
            echo '</pre>';
        }
    }

    public function oauth()
    {


        // instantiate the OAuth object
        // OAUTH_CONSUMER_KEY and OAUTH_CONSUMER_SECRET are constants holding your key and secret
        // and are always used when instantiating the OAuth object
        $oauth = new OAuth(
            _ETSY_APP_KEY_,
            _ETSY_SECRET_
        );

        try {
            // make an API request for your temporary credentials
            $req_token = $oauth->getRequestToken(
                _ETSY_API_URL_ . 'oauth/request_token?scope=',
                "oob"
            );

            //$this->_verifier($oauth,$req_token);
            echo '<form method="post" action="/index.php/c_test/verifier/">' .
                '<div>Код проверки</div>' .
                '<input type="text" value="" name="verifier"/><br/>' .
                '<input type="hidden" value="' . $req_token['oauth_token'] . '" name="oauth_token"/>' .
                '<input type="hidden" value="' . $req_token['oauth_token_secret'] . '" name="oauth_token_secret"/>' .
                '<input type="submit" value="OK" name="oauth_submit"/>' .
                '</form>';

            echo '<p>Перейдите по ссылке и введите в поле код проверки&nbsp;<a href="' . $req_token['login_url'] . '" target="_blank">' . $req_token['login_url'] . '</a></p>';

            echo '<pre>';
            print_r($req_token);
            echo '</pre>';

        } catch (OAuthException $e) {
            echo '<pre>';
            print_r($e);
            echo '</pre>';
            die();
        }

    }

}

?>