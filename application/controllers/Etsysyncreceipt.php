<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Контроллер синхронизации всех чеков
 * Class Etsysyncreceipt
 */
class Etsysyncreceipt extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
            'metsyauth',
            'mreceipt'
        ));

    }

    public function index($limit=100,$offset=0){

        $acc_token = $this->metsyauth->getAccessTokenFromDB();
        if( $acc_token != false){

            $oauth = new OAuth(_ETSY_APP_KEY_, _ETSY_SECRET_,
                OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
            $oauth->setToken($acc_token['oauth_token'], $acc_token['oauth_token_secret']);

            try {

                set_time_limit(30);

                $apiURI = "https://openapi.etsy.com/v2/shops/9138057/receipts?limit=".$limit.'&offset='.$offset;

                $data = $oauth->fetch($apiURI, null, OAUTH_HTTP_METHOD_GET);
                $json = $oauth->getLastResponse();

                $json = json_decode($json, true);

                //var_dump($json); die();
                if(is_array($json)){

                    $arCount = $json['count'];
                    $arResult = $json['results'];

                    if(is_array($arResult) && count($arResult)>0) {

                        $arLeft = $arCount;
                        $arOffset = $offset + $limit + 1;

                        if ($offset > 0) {
                            $arLeft = $arCount - $offset - 1;
                            $arOffset = $offset + $limit;
                        }

                        echo 'Всего записей: '.$arCount.'<br/>';
                        echo 'Текущая позиция: '.$offset.'<br/>';
                        echo 'Синхронизация данных...';

                        //var_dump($arResult); die();

                        foreach ($arResult as $ar) {

                            set_time_limit(30);

                            $data = array(
                                'receipt_id' => $ar['receipt_id'],
                                'order_id' => $ar['order_id'],
                                'seller_user_id' => $ar['seller_user_id'],
                                'buyer_user_id' => $ar['buyer_user_id'],
                                'creation_tsz' => $ar['creation_tsz'],
                                'last_modified_tsz' => $ar['last_modified_tsz'],
                                'name' => $ar['name'],
                                'first_line' => $ar['first_line'],
                                'second_line' => $ar['second_line'],
                                'city' => $ar['city'],
                                'state' => $ar['state'],
                                'zip' => $ar['zip'],
                                'formatted_address' => $ar['formatted_address'],
                                'country_id' => $ar['country_id'],
                                'payment_method' => $ar['payment_method'],
                                'payment_email' => $ar['payment_email'],
                                'buyer_email' => $ar['buyer_email'],
                                'seller_email' => $ar['seller_email'],
                                'shop_id' => $json['params']['shop_id']
                            );


                            $this->mreceipt->sync($data);
                        }

                        $this->index($limit,$arOffset);

                    } else {
                        echo 'Синхронизация окончена.';
                    }

                } else {
                    $this->load->view('oauth/error');
                }

                //print_r($json);

                foreach ($json['results'] as $item){
                    //var_dump($item); echo '<br/>';
                    break;
                }

            } catch (OAuthException $e) {
                error_log($e->getMessage());
                error_log(print_r($oauth->getLastResponse(), true));
                error_log(print_r($oauth->getLastResponseInfo(), true));
                exit;
            }


        } else {
            redirect(base_url().'etsyauth/','refresh');
        }

    }

}