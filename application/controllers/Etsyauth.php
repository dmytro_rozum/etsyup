<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Etsyauth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
            'metsyauth'
        ));

    }

    public function index(){

        $req_token = $this->metsyauth->AuthRequestToken();

        if($req_token != false){
            $this->load->view('oauth/oauth', $req_token);
        } else {
            $this->load->view('oauth/error');
        }

    }
}