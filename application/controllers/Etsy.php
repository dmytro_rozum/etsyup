<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Etsy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
            'metsyauth'
        ));

    }

    public function index($limit=1,$offset=0){

        $acc_token = $this->metsyauth->getAccessTokenFromDB();
        if( $acc_token != false){

            $oauth = new OAuth(_ETSY_APP_KEY_, _ETSY_SECRET_,
                OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
            $oauth->setToken($acc_token['oauth_token'], $acc_token['oauth_token_secret']);

            try {

                $apiURI = "https://openapi.etsy.com/v2/shops/9138057/receipts?limit=".$limit.'&offset='.$offset;

                $data = $oauth->fetch($apiURI, null, OAUTH_HTTP_METHOD_GET);
                $json = $oauth->getLastResponse();

                $json = json_decode($json, true);

                if(is_array($json)){
                    
                    $this->load->view('receipt', $json);

                } else {
                    $this->load->view('oauth/error');
                }

                //print_r($json);

                foreach ($json['results'] as $item){
                    //var_dump($item); echo '<br/>';
                    break;
                }

            } catch (OAuthException $e) {
                error_log($e->getMessage());
                error_log(print_r($oauth->getLastResponse(), true));
                error_log(print_r($oauth->getLastResponseInfo(), true));
                exit;
            }


        } else {
            redirect(base_url().'etsyauth/','refresh');
        }

    }

}
?>